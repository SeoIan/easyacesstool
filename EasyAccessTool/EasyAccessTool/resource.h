//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// EasyAccessTool.rc에서 사용되고 있습니다.
//
#define IDD_EASYACCESSTOOL_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDD_CONNECTIONWINDOW            130
#define IDC_GRP_CONTROL                 1000
#define IDC_BTN_CONNECTION_WINDOW       1001
#define IDC_BTN_START                   1002
#define IDC_GRP_VIEW                    1003
#define IDC_PC_VIEW                     1004
#define IDC_GRP_LOG                     1005
#define IDC_LTB_LOG                     1006
#define IDC_GRP_SET                     1007
#define IDC_GRP_INI                     1008
#define IDC_BTN_INI_LOAD                1009
#define IDC_BTN_INI_SAVE                1010
#define IDC_GRP_PARAMETER               1011
#define IDC_LB_EXPOSURE_MODE            1012
#define IDC_LB_EXPOSURE_TIME            1014
#define IDC_EDIT_EXPOSURE_TIME          1015
#define IDC_LB_GAIN                     1016
#define IDC_EDIT_GAIN                   1017
#define IDC_LB_TRIGGER_MODE             1018
#define IDC_LB_TRIGGER_SOURCE           1020
#define IDC_LB_WIDTH                    1022
#define IDC_EDIT_WIDTH                  1023
#define IDC_LB_HEIGHT                   1024
#define IDC_EDIT_HEIGHT                 1025
#define IDC_LB_OFFSET_X                 1026
#define IDC_EDIT_OFFSET_X               1027
#define IDC_LB_OFFSET_Y                 1028
#define IDC_EDIT_OFFSET_Y               1029
#define IDC_BTN_WRITE                   1030
#define IDC_BTN_READ                    1031
#define IDC_BTN_ADVANCED                1032
#define IDC_IP_PERSISTANT               1033
#define IDC_LTC_DEVICE                  1034
#define IDC_BTN_PERSISTNAT              1035
#define IDC_BTN_REFRESH                 1036
#define IDC_BTN_FORCEIP                 1037
#define IDC_BTN_CONNECT                 1038
#define IDC_GRP_AUTO_ALGORITHM          1039
#define IDC_LB_AUTO_EXPOSURE            1040
#define IDC_LB_AUTO_GAIN                1041
#define IDC_AUTO_TARGET_GREY            1042
#define IDC_LB_AUTO_WHITE_BALANCE       1043
#define IDC_SHARPNESS_ENABLE            1044
#define IDC_COMBO_EXPOSURE_MODE         1048
#define IDC_COMBO_TRIGGER_MODE          1049
#define IDC_COMBO_TRIGGER_SOURCE        1050
#define IDC_COMBO_AUTO_EXPOSURE         1051
#define IDC_COMBO_AUTO_GAIN             1052
#define IDC_COMBO_AUTO_TARGET_GREY      1053
#define IDC_COMBO_AUTO_WHITE_BALANCE    1054
#define IDC_CBTN_CROSS_HAIR             1055
#define IDC_CBTN_SHARPNESS              1056

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1057
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
